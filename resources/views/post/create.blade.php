@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('post.store', $post) }}" method="POST">
            @csrf
            @component('post.form', ['post' => $post])
            @endcomponent 
        </form>
    </div>
@endsection
