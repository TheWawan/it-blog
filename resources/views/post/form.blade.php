<div class="form-group">
    <label for="exampleInputEmail1">Title</label>
    <input type="text" class="form-control" name="title" value="{{ old('title', $post->title) }}">
</div>
<div class="form-group">
    <label for="exampleInputEmail1">Content</label>
    <input type="text" class="form-control" name="content" value="{{ old('content', $post->content) }}">
</div>
<button type="submit" class="btn btn-primary">Submit</button>
<a type="submit" href="{{ route('post.index') }}" class="btn btn-primary">Retour</a>
