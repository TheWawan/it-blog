@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('post.update', $post) }}" method="POST">
            @csrf
            @method('PUT')
            @include('post.form', ['post' => $post])
        </form>
    </div>
@endsection
