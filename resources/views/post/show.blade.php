@extends('layouts.app')

@section('content')
    <div class="container text-center">
      <div class="row">
        <div class="col-sm-5">
          <div class="clearfix"></div>
          <h2 class="section-heading">{{ $post->title }}</h2>
          <p class="lead">{{ $post->content }}</p>
        </div>
        <div class="col-lg-offset-1 col-sm-5">
          <img class="img-responsive" src="https://picsum.photos/500/300" alt="">
        </div>
      </div>
    </div>
@endsection
