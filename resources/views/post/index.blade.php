@extends('layouts.app')

@section('content')
<div class="album py-5 bg-light">
    <div class="container">
      <a href="{{ route('post.create') }}" class="btn btn-primary my-5">Create a post</a> 
      <div class="row">
        @foreach($posts as $post)
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="https://picsum.photos/200/210" alt="">
                <div class="card-body">
                  <h4 class="card-text">{{ $post->title }}</h4>
                  <p class="card-text">{{ $post->content }}</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <a href="{{ route('post.show', $post) }}" type="button" class="btn btn-sm btn-outline-secondary">View</a>
                      <a href="{{ route('post.edit', $post) }}" type="button" class="btn btn-sm btn-outline-secondary">Edit</a>
                      <a onclick="document.getElementById('delete-{{$post->id}}').submit()" type="button" class="btn btn-sm btn-outline-secondary">Delete</a>
                    </div>
                    <form action="{{ route('post.destroy', $post) }}" id="delete-{{$post->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                    </form>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection
