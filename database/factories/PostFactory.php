<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->realText($maxNbChars = 20, $indexSize = 2),
        'content' => $faker->realText($maxNbChars = 60, $indexSize = 2),
        'user_id' => 1
    ];
});
